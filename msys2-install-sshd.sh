#!/bin/sh -ex


PRIV_USER=sshd_server
PRIV_NAME="Privileged user for sshd"
UNPRIV_USER=sshd                                                                                    # DO NOT CHANGE; this username is hardcoded in the openssh code
UNPRIV_NAME="Privilege separation user for sshd"
CURRENT_USER=$(whoami)


EMPTY_DIR=/var/empty
WIN_EMPTY_PATH=$(/usr/bin/cygpath -w ${EMPTY_DIR})                                                  # will be used as HOME for the privileged sshd_server user
SSHD_EXE=$(/usr/bin/cygpath -w /usr/bin/sshd.exe)
ADMINGROUP="$(mkgroup -l | awk -F: '{if ($2 == "S-1-5-32-544") print $1;}')"
USERSGROUP="$(mkgroup -l | awk -F: '{if ($2 == "S-1-5-32-545") print $1;}')"


pacman -S --force --needed --noconfirm cygrunsrv inetutils mingw-w64-$(uname -m)-editrights openssh


mkdir -p ${EMPTY_DIR}
mkdir -p /var/log
touch /var/log/lastlog


#
# Check installation sanity
#
if ! /mingw64/bin/editrights -h >/dev/null; then
	echo "ERROR: Missing 'editrights'. Try: pacman -S mingw-w64-$(uname -m)-editrights"
	exit 1
fi

if ! cygrunsrv -v >/dev/null; then
	echo "ERROR: Missing 'cygrunsrv'. Try: pacman -S cygrunsrv"
	exit 1
fi

if ! ssh-keygen -A; then
	echo "ERROR: Missing 'ssh-keygen'. Try: pacman -S openssh"
	exit 1
fi


#
# The privileged sshd_server user. It will be owner of sshd service
#
TMP_PASS="$(tr -dc '[:alnum:]' < /dev/urandom | dd count=14 bs=1 2>/dev/null)"                      # Some random password; this is only needed internally by cygrunsrv and is limited to 14 characters by Windows

ADD="$(if ! net user "${PRIV_USER}" &>/dev/null; then echo "//add"; fi)"                            # Create user
if ! net user "${PRIV_USER}" "${TMP_PASS}" ${ADD} //fullname:"${PRIV_NAME}" //homedir:"${WIN_EMPTY_PATH}" //yes; then
	echo "ERROR: Unable to create Windows user ${PRIV_USER}"
	exit 1
fi

if ! (net localgroup "${ADMINGROUP}" | grep -q "^${PRIV_USER}$"); then                              # Add user to the Administrators group if necessary
	if ! net localgroup "${ADMINGROUP}" "${PRIV_USER}" //add; then
		echo "ERROR: Unable to add user ${PRIV_USER} to group ${ADMINGROUP}"
		exit 1
	fi
fi

passwd -e "${PRIV_USER}"                                                                            # Infinite passwd expiry

# set required privileges
for flag in SeAssignPrimaryTokenPrivilege SeCreateTokenPrivilege SeTcbPrivilege SeDenyRemoteInteractiveLogonRight SeServiceLogonRight; do
	if ! /mingw64/bin/editrights -a "${flag}" -u "${PRIV_USER}"; then
		echo "ERROR: Unable to give ${flag} rights to user ${PRIV_USER}"
		exit 1
	fi
done


#
# The unprivileged sshd user (for privilege separation). sshd expects this user in /etc/passwd file
#
ADD="$(if ! net user "${UNPRIV_USER}" &>/dev/null; then echo "//add"; fi)"
if ! net user "${UNPRIV_USER}" ${ADD} //fullname:"${UNPRIV_NAME}" //homedir:"${WIN_EMPTY_PATH}" //active:no; then
	echo "ERROR: Unable to create Windows user ${PRIV_USER}"
	exit 1
fi


#
# Add or update /etc/passwd entries
#
touch /etc/passwd
for u in "${PRIV_USER}" "${UNPRIV_USER}"; do
	sed -i "/^${u}:/d" /etc/passwd
	mkpasswd -l -u "${u}" | sed -E 's|(^.*:)(.*)|\1/bin/false|' >> /etc/passwd
done

mkpasswd -l -u "${CURRENT_USER}" >> /etc/passwd

# you need to invoke this command for domain user and it could take lot of time to walk through all domain users
#mkpasswd -d -u "${CURRENT_USER}" >> /etc/passwd


#
# Add or update /etc/group entries
#
touch /etc/group
for gr in "None" ${ADMINGROUP} ${USERSGROUP}; do
	mkgroup -l -g "${gr}" >> /etc/group
done

# you need to invoke this command for domain user and it could take lot of time to walk through all domain groups
# and put actual domain group name
#mkgroup -d -g "Domain Users" >> /etc/group


#
# Remove unprivileged sshd user, as we don't need it any more (we've it already added to /etc/passwd)
#
net user "${UNPRIV_USER}" //delete


#
# Make firewall exception rule
#
#netsh advfirewall firewall add rule name="SSHD allow" dir=in action=allow program="${SSHD_EXE}" enable=yes
netsh advfirewall firewall add rule name="Open port SSHD(22)" dir=in action=allow protocol=TCP localport=22

#
# Hide sshd_server user from users login screen
#
REG_KEY="HKLM\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon\\SpecialAccounts\\UserList"
REG ADD "${REG_KEY}" //f //v "${PRIV_USER}" //t REG_DWORD //d 0


#
# Finally, register service with cygrunsrv and start it
#
cygrunsrv -R sshd &>/dev/null || true
cygrunsrv -I sshd -d "MSYS2 sshd" -p /usr/bin/sshd.exe -a "-D -e" -y tcpip -u "${PRIV_USER}" -w "${TMP_PASS}"


#
# The SSH service should start automatically when Windows is rebooted. You can manually restart the service by running `net stop sshd` + `net start sshd`
#
if ! net start sshd; then
	echo "ERROR: Unable to start sshd service"
	exit 1
fi
