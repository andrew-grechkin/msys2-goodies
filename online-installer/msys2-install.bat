@echo off

cd "%~dp0"

set PATH_ORIG=%PATH%;
set PATH=%~dp0usr\bin;%SystemRoot%\System32

:: default target
set TARGET=c:\opt\msys
if NOT "%~1" == "" (
	set TARGET=%~1
)

:: create mount
mkdir "%TARGET%"
mklink /j mnt "%TARGET%"

:: create obligatory directories
mkdir mnt\dev\mqueue
mkdir mnt\dev\shm
mkdir mnt\etc
mkdir mnt\var\cache\pacman\pkg
mkdir mnt\var\empty
mkdir mnt\var\lib\pacman
mkdir mnt\var\log
mkdir mnt\tmp

icacls mnt\dev     /grant *S-1-5-11:(OI)(CI)F /T
icacls mnt\var\log /grant *S-1-5-11:(OI)(CI)F /T
icacls mnt\tmp     /grant *S-1-5-11:(OI)(CI)F /T

copy /y msys2-update-core.bat mnt
copy /y msys2-install-sshd.sh mnt

:: pacstrap
pacman -Sy --noconfirm --cachedir="mnt/var/cache/pacman/pkg" -r mnt base
::pacman -Sy --noconfirm -r mnt base

:: delete mount
rd mnt

:: do first run with configuration
set PATH=%PATH_ORIG%
set MSYS=winsymlinks:nativestrict
call "%TARGET%\msys2.exe"

::
::pacman -S --needed mingw-w64-x86_64-toolchain mingw-w64-x86_64-cmake

echo:
echo Installation is complete to %TARGET%
pause
