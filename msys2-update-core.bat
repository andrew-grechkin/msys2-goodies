@echo off

cd "%~dp0"

set PATH=%~dp0\usr\bin

pacman -Sy --force --needed --noconfirm msys2-runtime msys2-runtime-devel
